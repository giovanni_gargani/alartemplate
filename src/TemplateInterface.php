<?php
namespace Alar\Template;
use Psr\Http\Message\ResponseInterface;

interface TemplateInterface {
	/**
	 * Loads a variable into template engine
	 * @param string $key
	 * @param mixed $value
	 */
	public function param($key,$value);
	/**
	 * Loads an array into template engine, optional prefixing each key with $prefix
	 * @param array $lista Key/value hash of template variables. All keys must be string
	 * @param string $prefix Optional prefix will be prepended to all keys. A '_' will be added between prefix and key name
	 */
	public function paramArray(array $lista,$prefix='');
	/**
	 * Prints template on stdout
	 */
	public function echoOutput();
	/**
	 * Returns parsed template as a string.
	 * No debug output included
	 * @return string
	 */
	public function output();
	/**
	 * Set Template path
	 * @param string $path
	 */
	public function setTmplRoot($path);
	/**
	 * Sets Template filename
	 * @param string $filename
	 */
	public function setTmplFile($filename);
	/**
	 * Parses template and append it to a Response object stream
	 * @param ResponseInterface $Response
	 * @param string $fileName optional template name, overrides the one set with @see setFileName
	 * @param array $args Additional parameters, directly passed to @see paramArray
	 */
	public function render(ResponseInterface $Response,string $fileName=null,array $args=array());
	/**
	 *
	 * @param ResponseInterface $Response
	 * @param bool $force If true, debug window is shown already opened, otherwis a "Debug" link appears
	 */
	public function renderDebug(ResponseInterface $Response,bool $force);
	/**
	 * 
	 * Inject a dictionary object to process special dictionary keys and missing keys
	 * @param DictionaryInterface $obj
	 * @param string|null $prefix
	 */
	
	public function injectDictionary(DictionaryInterface $obj,$prefix=null);
	
	/**
	 * Inject a resolver objec that will be used to access templates sources
	 * @param ResolverInterface $obj
	 */
	
	function injectResolver(ResolverInterface $obj);
	    
}