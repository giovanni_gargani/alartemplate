<?php
namespace Alar\Template;


use Psr\Log\LoggerInterface;

trait ResolverTrait  {
    protected $classVersion=0;
    protected $cacheDir='/tmp/';

    /**
     *
     * {@inheritdoc}
     * @see \Alar\Template\ResolverInterface::resolve()
     */
    public function resolve(string $templatename, string $root = '', array $includePaths = null): string {
        if (substr($templatename, 0, 1) == DIRECTORY_SEPARATOR) {
            if ($this->logger instanceof LoggerInterface) $this->debug("Absolute $templatename");
        }
        
        if (@file_exists($templatename)) return realpath($templatename);
        $this->debug("Resolving [$root]/[$templatename]", $includePaths);
        foreach ($includePaths as $path) {
            if ($this->logger instanceof LoggerInterface) $this->debug("Trying $root$path$templatename");
            $found = realpath("$root$path$templatename");
            if ($found) return $found;
        }
        return realpath("$root$templatename");
    }

    /**
     *
     * {@inheritdoc}
     * @see \Alar\Template\ResolverInterface::getTemplate()
     */
    public function getTemplate(string $filename): string {
        return @file_get_contents($filename);
    }

    /**
     *
     * {@inheritdoc}
     * @see \Alar\Template\ResolverInterface::peekTemplate()
     */
    public function peekTemplate(string $filename): bool {
        return @file_exists($filename);
    }

    /**
     *
     * {@inheritdoc}
     * @see \Alar\Template\ResolverInterface::getCompiledFileName()
     */
    public function getCompiledFilename(string $filename): string {
        return $this->buildCompiledFilename($this->cacheDir, $filename, $this->classVersion);
    }
    
    protected function buildCompiledFilename(string $cacheDir,string $filename,string $version) {
        return sprintf("%s%s.v%s.php",$cacheDir,$filename,$version);
    }
        
    // "Class rewritten. All templates non fresh ({$fname})"
    // sprintf("Compiled template %s missing or not fresh. Compiled: %s Source: %s",$fname,gmdate('c',$timeCompiled),gmdate('c',$timeSource))
    
    /**
     *
     * {@inheritdoc}
     * @see \Alar\Template\ResolverInterface::isFresh()
     */
    public function isFresh(string $filename): bool {
        $s=@filemtime($filename);
        $c=@filemtime($this->getCompiledFileName($filename));
        if ($s and $c and $s<$c) return true;
        // No compiled template found for current version, let's remove old ones
        $glob=$this->buildCompiledFilename($this->cacheDir, $filename, '*');
        $deletables=glob($glob);
        foreach ($deletables as $deletable) {
            @unlink($deletable);
        }
        return false;
    }

    public function saveCompiledTemplate(string $filename, string $content): bool {
        return @file_put_contents($this->getCompiledFilename($filename),$content)!==false;
    }

    public function setClassVersion(string $version) {
        $this->version=$version;
    }
    public function setCachedir(string $path) {
        $candidate=rtrim($path,DIRECTORY_SEPARATOR). DIRECTORY_SEPARATOR; // Makes sure we have one and only one trailing slash
        if (!@is_dir($this->cacheDir)) {
            if (!@mkdir($candidate,0755,true)) if ($this->logger instanceof LoggerInterface) $this->warning("Unable to create $candidate");
        }
        elseif (!is_writable($candidate)) if ($this->logger instanceof LoggerInterface) $this->warning("Cant write to $candidate");
        $this->cachedir=$candidate;
        if ($this->logger instanceof LoggerInterface) $this->debug("Cachedir changed to $candidate");
    }
    public function getCompileTime(string $filename) : int {
        return intval(@filemtime($this->getCompiledFilename($filename)));   
    }

}