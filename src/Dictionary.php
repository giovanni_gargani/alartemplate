<?php
namespace Alar\Template;
class Dictionary implements DictionaryInterface {
	private $dict=array();
	/**
	 * Load dictionary from hash or file
	 * @param null|string|array $hash
	 * @param string fileType can only be json, meaningless if $hash is not a filename
	 * 
	 */
	
	function __construct($hash=null,$fileType='json') {
		if (is_array($hash)) {
			$this->load($hash);
		}
		elseif(is_string($hash)) {
			$fname=realpath($hash);
			if ($fname) {
				if ($fileType=='json') {
					$this->loadJsonFile($fname);
				}
				elseif (strlen($fileType==1)) {
					$this->loadDelimitedFile($fname,$fileType);
				}
			}
		}
	}
	function get($event,$key,$context) {
		if ($event==template::DICT) {		
			return $this->dict[strtoupper($key)];
		}
		else {
			return null;
		}
	}
	function set($key,$value) {
		$this->dict[strtoupper($key)]=$value;
	}
	public function getIterator() {
		return new \ArrayIterator($this);
	}
	/**
	 * Directly receive a hash.
	 * Will convert all keys to UPPER CASE
	 * @param array $hash
	 */
	public function load(array $hash) {
		array_merge($this->dict,$hash);
		$this->dict=array_change_key_case($this->dict,CASE_UPPER);
	}
	/**
	 * Specialized function to load a json file
	 * @param string $fname
	 */
	public function loadJsonFile($fname) {
		$this->load(@json_decode(@file_get_contents($fname),true));
		
	}
	/**
	 * 
	 * @param string $fname Absolute filename
	 * @param string $delimiter delimiter used to separate key from value
	 */
	public function loadDelimitedFile($fname,$delimiter) {
		$data=file($fname,FILE_IGNORE_NEW_LINES); 
		foreach ($data as $line) {
			list($k,$v)=explode($delimiter,$line);
			$this->dict[strtoupper($k)]=$v;
		}
	}
	
	
}