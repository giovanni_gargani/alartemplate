<?php
namespace Alar\Template;
trait TemplateAwareTrait {
	/**
	 * The template instance.
	 *
	 * @var TemplateInterface
	 */
	protected $template;
	function setTemplate(TemplateInterface $template) {
		$this->template=$template;
	}

}