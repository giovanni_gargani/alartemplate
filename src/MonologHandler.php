<?php
namespace Alar\Template;
if (!interface_exists('\Monolog\Handler\HandlerInterface',true)) {
	throw new \Exception(__NAMESPACE__ . '\MonologHandler needs monolog/monolog');
}
use Monolog\Logger;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\AbstractProcessingHandler;
use Alar\Template\Template;
class MonologHandler extends AbstractProcessingHandler {
	/**
	 * @var template
	 */
	private $template;
	private $map=array(

	);
	public function __construct (Template $template,$level = Logger::DEBUG, $bubble = true) {
		$this->template=$template;
		// Disables external logger to avoid recursion
		$template->setLogger(null);
		parent::__construct($level,$bubble);
	}
	public function write(array $record) {
		error_log($record['level'].' '. $record['message']);
		// Makes use of data from IntrospectorProcessor if present
		if ($record['extra']['line']) {
			$message=sprintf("%s in %s:%d",$record['message'],'...' . substr($record['extra']['file'],-30),$record['extra']['line']);
		}
		else {
			$message=$record['message'];
		}
		$this->template->mark($record['level_name'],$message,$record['context']);
	}
	public function xisHandling(array $record) {
		error_log($this->level . ' vs ' . $record['level']);
		return true;
	}
}