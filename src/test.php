<?php
ini_set("display_errors","on");
error_reporting(E_ALL ^E_NOTICE);
require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload
use alar\template\template;
use alar\template\dictionary;
class mydictionary extends dictionary {
	function get($event,$key,$context) {
		if ($event==template::DICT) {
			return 'we could set up an external dictionary and have it be called via callback';
		}
		else {
			return "Missing data for $key";
		}
	}
}
try {
	$debugLevel=template::MARK_FULL;
	//$debugLevel=alartemplate::MARK_FULL;
	$options=array(
		'debug'=>true,
		'debugLevel'=>$debugLevel,
		'debugInclude'=>"1",
		'callback'=>new mydictionary(),
		'filename'=>'test.html',
		);
	/*
	 * Calling contructor with $options array or invoking setter after "new"
	 * is equivalent
	 */
	if ($_GET['nodebug']) $options['debug']=false;
	$number=rand(0,1);
	if ($number) {
		$tmpl=new template();
		$tmpl->setDebug(true);
		$tmpl->setDebugLevel($debugLevel);
		$tmpl->setDebugInclude(1);
		$tmpl->setCallback(new mydictionary());
		$tmpl->setFileName("test.html");
		$tmpl->param('method',"Inited via setters");
	}
	else {
		$tmpl=new template($options);
		$tmpl->param('method',"Inited via options array");
	}
	$tmpl->setNoCompile(true);
	$tmpl->param("WELCOME","AlarTemplate class used: ");
	$tmpl->addParam(array("GIRO"=>array(array("Nome"=>"pippo"),
										array("Nome"=>"pluto"),
										array("Nome"=>"paperino"))));

	$tmpl->addParam("GIRetto",array(array("nome"=>"pippo"),
										array("nome"=>"pluto"),
										array("nome"=>"paperino")));
	$tmpl->echoOutput();
}
catch (exception $e) {
	echo "<pre>";
	echo $e->getMessage();
	echo "</pre>";
}

?>
