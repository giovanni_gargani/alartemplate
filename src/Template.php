<?php
namespace Alar\Template;
use \Exception as Exception;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Http\Message\ResponseInterface;

// BEGIN DEBUG
################################################################################
# PHP-HTML::Template                                                           #
# http://alarphptemplate.sourceforge.net/                                      #
################################################################################
# A template system for PHP based on HTML::Template Perl Module                #
# Version 1.1.0                                                                #
# 15-APR-2017                                                                  #
# See file README for details                                                  #
################################################################################
# Author: Giovanni Gargani, giovanni@gargani.it                                #
# License: GNU LGPL (included in file "LICENSE")                               #
# (c) 2009 by Giovanni Gargani                                                 #
################################################################################
# HTML::Template Perl module copyright by Sam Tregar                           #
################################################################################
# Require at least php 7.0
/* ************** *
 * TEMPLATE CLASS *
 * ************** */
// END DEBUG
 /*
  * At this time it supports:
  * - <TMPL_VAR NAME=varname>
  *		(An optional parameter ESCAPE can be added to escape var for HTML, JS ,URL or JSON)
  * - <TMPL_LOOP NAME=varname></TMPL_LOOP> OK
  * - <TMPL_IF NAME=varname>[<TMPL_ELSEIF NAME=varname>...][<TMPL_ELSE>]</TMPL_IF> OK
  * - <TMPL_UNLESS NAME=varname>[<TMPL_ELSEUNLESS NAME=varname>...][<TMPL_ELSE>]</TMPL_UNLESS> OK
  * - <TMPL_INCLUDE NAME=varname> OK
  * - <TMPL_STATICINCLUDE NAME=varname>
  *		(Include a file without processing it)
  * - <TMPL_VAR_XML VERSION=version ENCODING=encoding>
  *		(Make a XML PROLOGUE )
  * See README for detailed info.
  */
class LogLevel extends \Psr\Log\LogLevel {
	const SYSTEM = "system";
	const UNKNOWN ="unknown";
	const SUCCESS ="success";
	const TEST="test";
	const CONFIG="config";
	static $startSec=0;
	const LEVELS=[
			self::EMERGENCY=>"<span class='purple'>%s - %s</span>",
			self::CRITICAL=>"<span class='purple'>%s - %s</span>",
			self::TEST=>"<span class='purple'>%s - %s</span>",
			self::ALERT=>"<span class='purple'>%s - %s</span>",
			self::ERROR=>"<span class='red'>%s - %s</span>",
			self::WARNING=>"<span class='orangered'>%s - %s</span>",
			self::SUCCESS=>"<span class='green'>%s - %s</span>",
			self::NOTICE=>"<span class='darkgreen'>%s - %s</span>",
			self::INFO=>"<span class='blue'>%s - %s</span>",
			self::DEBUG=>"<span class='black'>%s - %s</span>",
			self::UNKNOWN=>"<span class='silver'>%s - %s</span>",

	];
	const ALERTS="alerts";
	const FINALS="finals";
	const EVENTS="events";
	const HEADERS="headers";
	static function decore($level,$message) {
		$level=strtolower($level);
		if (array_key_exists($level,self::LEVELS)) {
			$format=self::LEVELS[$level];
		}
		else {
			$format=self::LEVELS[self::UNKNOWN];
		}
		return sprintf($format,strtoupper($level),$message);
	}
	static function class($level) {
		switch(strtolower($level)) {
			case self::EMERGENCY:
				return self::HEADERS;
			case self::ALERT:
				return self::ALERTS;
			case self::CRITICAL:
				return self::FINALS;
			default:
				return self::EVENTS;
		}
	}

}
LogLevel::$startSec=$_SERVER['REQUEST_TIME_FLOAT']?$_SERVER['REQUEST_TIME_FLOAT']:microtime(true);
class Template implements LoggerInterface,LoggerAwareInterface,TemplateInterface {
	use LoggerTrait,LoggerAwareTrait;
	const DICT='dict';
	const MISS='miss';
	const VERSION="1.0";
	private $errLevelMap;

	private $optionsMap=array(
			'cachedir'=>'setCacheDir',
			'dictionary'=>'injectDictionary',
	        'resolver'=>'injectResolver',
			'debug'=>'setDebug',
			'debuginclude'=>'setDebugInclude',
			'debuglevel'=>'setDebugLevel',
			'filename'=>'setTmplRoot',
	        'dictprefix'=>'setDictPrefix',
			'forwardinput'=>'setForwardInput',
			'logger'=>'setLogger',
			'nodict'=>'setNoDict',
			'nocompile'=>'setNoCompile',
			'novars'=>'setNoVars',
			'paths'=>'setPaths',
			'tmplroot'=>'setTmplRoot',
	);
	protected $dbgMessages=array();
	private $options=array();
	private $data=array();
	private $testMode=true;
	private $includes=array();
	private $tree=array();
	private $treeLevel=0;
	private $unique=0;
	private $currentContexts=array();
	private $cacheDir="/tmp/cache/"; // Default dir for compiled template cache
	private $timers=array();
	private $output=null;
	private $_regExp='/<(\/?)(tmpl_[^ >]+) *([^>]*?)-{0,2}\/?>/i'; // Main regexp....simple but powerful
	private $regExp='';
	private $debugTemplate=false; //Show debug informations
	private $debugVars=false; //Print keys instead of values for standard keys
	private $debugDict=false; //Prints keys instead of values for dictionary keys
	private $includePaths=array(); //Array of paths to be searchd for template. Basedir of main template is always searched
	private $dictPrefix="DICT_";
	private $debugLevel=1;
	private $mustRefresh; //Filemtime of class. Used to
	private $templateRoot=''; //Default base dir (include path can be relative to this one)
	private $templateFile;
	private $templateSearchOnInclude=true;
	private $templateIncludePaths=array();
	private static $numericErrorLevel=array(
			LogLevel::EMERGENCY => 1,
			LogLevel::ALERT     => 2,
			LogLevel::CRITICAL  => 3,
			LogLevel::ERROR     => 4,
			LogLevel::WARNING   => 5,
			LogLevel::NOTICE    => 4,
			LogLevel::INFO      => 7,
			LogLevel::DEBUG     => 8,
			"success"			=> 4,
			"none"				=> 0,
	);
	/** @var DictionaryInterface */
	private $dictionary;
	/** @var ResolverInterface */
	private $resolver;
	
	/**
	 * Ps-3 Interface
	 * {@inheritDoc}
	 * When used with no logger defined switch back to internal implementation
	 * @see \Psr\Log\LoggerInterface::log()
	 */
	public function log($level, $message, array $context = array()) {
		if (!$this->debugTemplate) return; //We are not interested in template logging at all
		if ($this->logger) {
			// Log level test is deletgated to the outside logger
			$this->logger->log($level,$message,$context);
		}
		else {
			// In this case,we need to test our internal log level
			if (self::numeric($level)>$this->debugLevel+4) return;
			$this->mark($level,$message,$context);
		}
	}
	public function setLogger(\Psr\Log\LoggerInterface $logger=null) {
	    if ($logger) {
    		$this->logger=$logger;
    		$this->resolver->setLogger($logger);
	    }
	}
	public function version() {
		return self::VERSION;
	}
	public function __construct($options=array()) {
		$this->regExp=$this->_regExp;
		if (is_array($options)) {
			$this->init($options);
		}
		else {
			$this->setPaths([$options]; //slim php-view style initialization
		}
		this->debug("Regexp=".htmlspecialchars($this->regExp));
		$this->mustRefresh=filemtime(__FILE__);
		$this->resolver=new Resolver($this->mustRefresh,$this->logger);
	}
	/**
	 * Converts PSR-3 log states in a numeric log level
	 *
	 * @param string|int Level number (monolog) or name (PSR-3)
	 * @return int
	 */

	public static function numeric($level)
	{
		if (is_numeric($level)) {
			return intval($level);
		}
		elseif (is_string($level)) {
			return self::$numericErrorLevel[$level];
		}
		return 95;
	}

	public function __destruct() {
		unset($this->dictionary);

	}
	private function h2($msg,$color='green') {
		printf('<div class="markTable %s">%s</div>'."\n",$color,$msg);
	}
	private function h3($title,$msgs,$color='purple') {
		printf('<div class="markTable %s">%s<div class="markDetails">',$color,$title);
		echo implode('<br/>',(array)$msgs);
		echo "</div></div>\n";
	}
	private function f2($msg,$color='green') {
		printf('<span class="%s">%s</span><br/>'."\n",$color,$msg);
	}
	private function h4($title,$msgs,$color='purple') {
		printf('<div class="markTable %s">%s',$color,$title);
		echo $this->getToggleHeader();
		echo "<pre>";
		echo print_r($msgs,1);
		echo "</pre></div></div>\n";
	}
	private function f4($title,$msgs,$color="green") {
		printf('<span class="%s">%s',$color,$title);
		echo $this->getToggleHeader();
		echo "<pre>";
		echo print_r($msgs,1);
		echo "</pre></div></span><br/>\n";
	}
	function getToggleHeader($name='Toggle') {
		static $counter=0;
		$id=sprintf('ALARTEMPLATEHEADER_%d',$counter++);
		$link=" <a class=\"opentoggle\" id=\"open{$id}\" href=\"#\" onClick=\"return alartemplatedebughelper.toggle('{$id}','none');\">$name</a>";
		$link.="<a class=\"closetoggle\" id=\"close{$id}\" href=\"#\" onClick=\"return alartemplatedebughelper.toggle('{$id}','block');\">$name</a>";
		$link.="<div class=\"toggable\" id=\"{$id}\">";
		return $link;
	}
	protected function finalDebug(bool $force=false) {
		if (!empty($this->dbgMessages)) {
			ob_start();
			$this->jsProlog();
			$this->cssProlog();
			echo $this->dbgOpen($force);
			$this->finalDebugHeader();
			if (!empty($this->dbgMessages[LogLevel::EVENTS])) {
    			echo "<pre>Event log\n",@implode("\n",$this->dbgMessages[loglevel::EVENTS]),"</pre>\n";
			}
			$this->finalDebugFooter();
			echo $this->dbgClose();
    		return ob_get_clean();
		}

	}
	protected function finalDebugHeader() {
		$this->h2("Showmark level:" .$this->debugLevel);
		$this->h2("Start time: " .LogLevel::$startSec);
		@$this->h2(sprintf('Elapsed time  (without debug output) %.6f' , (microtime(true)-LogLevel::$startSec)),'navy');
		foreach ((array)$this->dbgMessages[loglevel::ALERTS] as $msg) {
			if (is_array($msg)) {
				$this->h4($msg[0],$msg[1],'red');
			}
			else {
				$this->h2($msg,'red');
			}
		}
		unset($this->dbgMessages[loglevel::ALERTS]);
		$this->h2('Php Version: ' . phpversion(),'black');
		$this->h2('User agent: ' . $_SERVER['HTTP_USER_AGENT'],'black');
		$this->h2('Document root: ' . $_SERVER['DOCUMENT_ROOT'],'black');
		$this->h2('Host: ' . $_SERVER['HTTP_HOST'],'black');
		$this->h2('Script: ' . $_SERVER['SCRIPT_FILENAME'],'black');
		$this->h2('Include path: ' . get_include_path(),'black');
		if ($_SERVER['AKAMAI_EDGE_IP']) {
			$this->h2("Akamai: served via {$_SERVER['AKAMAI_EDGE_IP']}");
			$this->h2('Akamai: generated on ' . date('r'));
		}
		else {
			$this->h2("Akamai: disabled");
		}
		ksort($_SERVER);
		$this->h4('Template Chain',array(
				'HardBase'=>TMPL_ROOT,
				'Base'=>$this->templateRoot,
				'Paths'=>$this->templateIncludePaths),
				'navy'
				);
		$this->h4('Environment:',$_SERVER,'navy');
		if (function_exists('posix_uname')) {
			$this->h4('Node:',posix_uname(),'navy');
		}
		$this->h4('Cookie:',$_COOKIE,'navy');
		if (function_exists('eaccelerator_info')) {
			$eAccelerator=eaccelerator_info();
			$this->h2("eAccelerator is active: version " . $eAccelerator['version'],'green');
			$this->markFinal("eAccelerator info",false,$eAccelerator);
		}
		else {
			$this->h2("eAccelerator is NOT active",'red');
		}
		$this->h4('Http headers sent:',headers_list(),'purple');
		foreach ((array)$this->dbgMessages[LogLevel::HEADERS] as $msg) {
			if (is_array($msg)) {
				$hcolor='navy';
				if ($msg[2]) $hcolor=$msg[2];
				$this->h4($msg[0],$msg[1],$hcolor);
			}
			else {
				$this->h2($msg,'navy');
			}
		}
		unset($this->dbgMessages[LogLevel::HEADERS]);

	}
	protected function finalDebugFooter() {
		echo '<div id="markfinal" class="markTable">Finals<br/>'."\n";
		foreach ((array)$this->dbgMessages[loglevel::FINALS] as $msg) {
			if (is_array($msg)) {
				$hcolor='navy';
				if ($msg[2]) $hcolor=$msg[2];
				$this->f4($msg[0],$msg[1],$hcolor);
			}
			else {
				$this->f2($msg,'navy');
			}
		}
		$this->f4('Template data',$this->dump());
		$this->f4('Include templates',$this->tree());
		echo '</div>';
	}
	protected function raiseError($method,$message) {
		if ($this->useException)
			throw new Exception(get_class()."::$method - $message");
		else
			trigger_error(get_class()."::$method - $message",E_USER_ERROR);
	}

	protected function fire($event,$name,$context) {
		$this->debug("Called fire",func_get_args());
		if ($this->dictionary instanceof dictionaryInterface) {
			return $this->dictionary->get($event,$name,$context);
		}
		else
			return $default;
	}
	/**
	 * Outputs a log message to the internal log system
	 * @param string $level PSR-3 Log level
	 * @param string $msg
	 * @param array $data
	 */
	public function mark(string $level,$msg,array $data=array(),$caller='') {
		static $unique=0;
		$unique++;
		$s=LogLevel::decore($level,$msg);
		$class=loglevel::class($level);
		if ($class==loglevel::EVENTS) {
			if (!empty($data)) {
					$s.="<a href=\"#\" onclick=\"return alartemplatedebughelper.toggle('alartemplatedbg$unique','block')\">Toggle</a><blockquote style=\"display:none\" id=\"alartemplatedbg$unique\">".print_r($data,1).'</blockquote>';
			}
			$s=sprintf("%.3f %s",microtime(true) - LogLevel::$startSec,$s);
		}
		else {
			if (!empty($data)) {
				$s=[$msg,$data];
			}
			else {
				$s=$msg;
			}
		}
		$this->dbgMessages[$class][]=$s;
	}
	function success($msg,array $data=array()) {
		$this->debug($msg,$data);
	}

	protected function dbgOpen($force=false) {
		if ($force)
			return '<div id=alartemplatedebughelperid>'."\n";
		else
			return
	'<div style="text-align:center;background-color:silver;clear:both;font-family:arial;color:navy"><hr><a href=# onclick="return alartemplatedebughelper.toggle(\'alartemplatedebughelperid\')">Debug</a></div>' .
	'<div style="display:none" id="alartemplatedebughelperid">'."\n";
	}

	protected function dbgClose() {
		return "\n</div>";
	}
	public function __get($nome) {
		$nome=strtolower($nome);
		switch ($nome) {
			case "output": return $this->output;
			default: return null;
		}
	}
	public function __call($method,$args) {
		if (!strncasecmp($method,"TMPL",4)) {
			$this->alert("Unknown tag $method",$args);
		}
		else {
			$this->critical("Unknown method $method",$args);
			throw new Exception("Missing method $method.");

		}
	}

	protected function inlineDebug($msg) {
      print '<div style="margin: 10px 30px; padding: 5px; border: 2px solid black; background: gray; color: white; font-size: 12px; font-weight: bold;">';
      echo $msg;
      print '</div>';
	}
	protected function cssProlog() {
	static $done=false;
	if ($done) return;
	$done=true;
$testo=<<<HEREDOC
	<style type="text/css">
	#alartemplatedebughelperid {font-family: "Trebuchet MS",sans-serif; border: thin solid black;}
	#alartemplatedebughelperid pre {font-family: "Trebuchet MS",sans-serif;font-size:13px;margin-bottom:6px; text-align:left ; background-color: lightyellow ; color:black}
    #alartemplatedebughelperid a {color:#000;text-decoration:underline;}
    #alartemplatedebughelperid a:hover {color:#c00;text-decoration:underline;}

    #alartemplatedebughelperid .silver {color:silver;}
    #alartemplatedebughelperid .gray {color:gray;}
    #alartemplatedebughelperid .navy {color:navy;}
    #alartemplatedebughelperid .orangered {color:darkorange;}
    #alartemplatedebughelperid .red {color:red;}
    #alartemplatedebughelperid .green {color:green;}
    #alartemplatedebughelperid .purple {color:purple;}
    #alartemplatedebughelperid .teal {color:teal;}
    #alartemplatedebughelperid .black {color:black;}
    #alartemplatedebughelperid .blue {color:navy;}

    div.toggable {display:none;}
    a.closetoggle {display:none;}
    a.opentoggle {display:inline;}

    div.markDetails {padding:1px 25px;}
    .markTable {background-color:#CCCCCC;border-bottom:1px solid #AAAAAA;font-size:13px;margin:2px 0 0;padding:1px 5px;text-transform:none;text-align:left;}

    #markFinal {background-color: #F0F0F0;text-align:left;}
    #markShow {background-color:LightYellow;text-align:left;}
	</style>
HEREDOC;
	echo $testo;
	}
	protected function jsProlog() {
		static $done=false;
		if ($done) return;
		$done=true;
$testo=<<<HEREDOC
<script type="text/javascript">
var alartemplatedebughelper={

getStyle: function (elem, style) {
  if (elem.getStyle) {
    return elem.getStyle(style);
  } else {
    return elem.style[style];
  }
}
,
setStyle: function(elem, style, value) {
  if (elem.setStyle) {
    elem.setStyle(style, value);
  } else {
    elem.style[style] = value;
  }
}
,
toggle: function(id) {
    var pre = document.getElementById( id);
    if (pre) {
      if (this.getStyle(pre, 'display') == 'block') {
        this.setStyle(pre, 'display', 'none');
      } else {
        this.setStyle(pre, 'display', 'block');
      }
    }
  	return false;
  }
}
</script>
HEREDOC;
	echo $testo;
	}

	protected function init($options) {
		$this->debug("Template init",$options);
		$this->options=array_change_key_case($options);
		foreach ($this->options as $option=>$value) {
			$method=$this->optionsMap[$option];
			$this->$method($value,$option);
		}
	}

	/* Helpers
	 *
	 */
	function getInput($var) {
		return $_REQUEST[$var];
	}
	/**
	 * Prints template on stdout
	 */
	public function echoOutput() {
		if ($this->output)
			echo $this->output;
		else
			echo $this->output();
		$this->showFinalDebug();
	}

	protected function resolveFile($fname,$root='',$includePaths=array()) {
	    if (empty($root)) $root=$this->templateRoot;
        if (empty($includePaths)) $includePaths=(array)$this->templateIncludePaths;
	    $found=$this->resolver->resolve($fname,$root,$includePaths);
/*	    
		if (substr($fname,0,1)== DIRECTORY_SEPARATOR) {
			$this->debug("Absolute $fname");
		}
		if (empty($root))
			$root=$this->templateRoot;
		if (empty($includePaths))
			$includePaths=(array)$this->templateIncludePaths;
		if ($this->file_exists($fname))
			return $this->realpath($fname);
		$this->debug("Resolving $fname",$includePaths);
		foreach ($includePaths as $path) {
			$this->debug("Trying $root$path$fname");
			$found=$this->realpath("$root$path$fname");
			if ($found) return $found;
		}
		$found=$this->realpath("$root$fname");
*/		
		if ($found) return $found;
		$this->Error("Template $fname not found");
		return $fname;
	}


	/*
	 * Configuration method. Array format option is remapped to method
	 * based configuration
	 */

	function setPaths(array $path) {
		$this->templateIncludePaths=array();
		foreach((array)$path as $p) {
			$p=$this->realpath($p);
			if ($p)
				$this->templateIncludePaths[]= "$p/";
		}

	}
	/**
	 * Sets Template path
	 * @param string $path
	 */
	public function setTmplRoot($path) {
		$this->templateRoot=$path;
	}
	/**
	 * Gets Template path
	 *
	 */
	public function getTmplRoot() {
		return $this->templateRoot;
	}
	/**
	 * Sets Template filename
	 * @param string $filename
	 */
	public function setTmplFile($filename) {
		$this->templateFile=$filename;
	}
	/**
	 * Gets Template filename
	 * @param string $filename
	 */
	public function getTmplFile() {
		return $this->templateFile;
	}
	function setCacheDir($path=null) {
		$this->cacheDir=$path;
	}
	function setNoCompile($flag) {
		$this->noCompile=$flag;
		if ($this->noCompile) {
			$this->mustRefresh=time();
			$this->Warning("Template compilation disabled");
		}
	}
	/**
	 * Setting to false completely disable template info logging
	 * @param bool $flag
	 */
	function setDebug($flag) {
		$this->debugTemplate=(Boolean)$flag;
		if ($flag and $this->debugLevel==0) {
		  $this->debugLevel=self::numeric(\Psr\Log\LogLevel::INFO);
	    }
	}
	/**
	 * Set logging level
	 * @param string $flag PSR-3 log level
	 */
	function setDebugLevel($flag) {
		$this->debugLevel=self::numeric($flag);
		$this->debugTemplate=(Boolean)$this->debugLevel;

	}
	/**
	 * Setting to false completely disables logging information about included file
	 * @param bool $flag
	 */
	function setDebugInclude($flag) {
		$this->debugInclude=(Boolean) $flag;

	}
	/**
	 * Enables var debug: engine will print variable names instead of their values
	 * @param boolean $flag
	 */
	function setNoVars($flag) {
		$this->debugVars=$flag;
	}
	/**
	 * Enables dictinoary debug: engine will print dictionary names instead of their values
	 * @param boolean $flag
	 */
	function setNoDict($flag) {
		$this->debugDict=$flag;
	}
	/**
	 * 
	 * {@inheritDoc}
	 * @see \Alar\Template\TemplateInterface::injectDictionary()
	 */
	function injectDictionary(DictionaryInterface $obj,$prefix=null) {
	    $this->dictionary=$obj;
	    if (!is_null($prefix)) $this->setDictPrefix($prefix);
	}
	/**
	 * 
	 * {@inheritDoc}
	 * @see \Alar\Template\TemplateInterface::injectResolver()
	 */
	function injectResolver(ResolverInterface $obj) {
	    $this->resolver=$obj;
	    $this->resolver->setClassVersion(self::VERSION);
	}
	/**
	 * Changes the dictionary prefix
	 * @param string $prefix (default prefix is 'DICT_'
	 */
	function setDictPrefix($prefix="DICT_") {
	    $this->dictPrefix=$prefix;
	}
	/**
	 * Returns parsed template as a string.
	 * If no template file is defined return an empty string
	 * No debug output included
	 * @return string
	 */
	public function output() {
		if (empty($this->output)) {
		    if ($this->templateFile) {
    			ob_start();
    			$this->_output();
    			$this->output=ob_get_clean();
		    }
		    else {
		        $this->_output="";
		    }
		}
		return $this->output;
	}
	/**
	 *
	 * @param Response $response
	 * @param string $templateName
	 * @param array $args
	 * @return \Psr\Http\Message\ResponseInterface
	 * @see output
	 */
	public function render(ResponseInterface $response, string $templateName=null,array $args=[]) {
		if (is_string($templateName)) {
			$this->setTmplFile($templateName);
		}
		foreach ($args as $key=>$value) {
			$this->param($key, $value);
		}
		return $response->getBody()->write($this->output());
	}
	public function renderDebug(ResponseInterface $response,bool $force=false) {
		return $response->getBody()->write($this->finalDebug($force));
	}
	function resetParams() {
		$this->resetTemplate();
		$this->data=array();
	}
	function resetTemplate() {
		$this->output=null;
	}
	private function _output() {
		$start=microtime(true);
		$this->rootData=$this->newNode("root",$this->data,1,1);
		$this->tree[0]=array('level'=>$this->treelevel++,'requested'=>$this->templateFile);
		$fname=$this->resolveFile($this->templateFile);
		$this->tree[0]['included']=$fname;
		if (substr($fname,0,1)!='/') {
			$tag="INITIAL_SMARTINCLUDE NAME=$fname";
			$fname=$this->resolveFile($fname);
		}
		else {
			$tag="INITIAL_INCLUDE NAME=$fname";
		}
		$s=$this->CompileTemplate($fname,$tag);
		$this->debug("Template ouput done in ". $this->elapsed($start));
	}

	function dump() {
		return $this->data;
	}

	protected function addParam($key,$value=null) {
		if (func_num_args()==1) {
			if (is_array($key))
				$this->paramArray($key);
			else
				$this->raiseError(__FUNCTION__,"Only one param and not an associative array");
		}
		elseif (func_num_args()==2) {
			if (is_array($key))
				$this->raiseError(__FUNCTION__,"Two params but first one is an array");
			else
				if (is_array($value)) {
					$value=array_change_key_case($value,CASE_UPPER);
				}
				$this->data[strtoupper($key)]=$value;

		}
	}
	/**
	 * Checks if the key is present in the engine data
	 * @param string $key
	 * @return boolean
	 */
	public function hasParam($key) {
		return array_key_exists(strtolower($key), $this->data);
	}
	/**
	 * Returns loaded parameters hash
	 * @return array
	 */
	public function getParams() {
		return $this->data;
	}
	/**
	 * Loads a variable into template engine
	 * @param string $key
	 * @param mixed $value
	 */
	public function param($key,$value) {
		$this->addParam($key,$value);
	}
	/**
	 * Loads a variable into template engine but only if not already existent
	 * @param string $key
	 * @param mixed $value
	 */
	public function paramOpt($key,$value) {
		if (!$this->hasParam($key)) {
			$this->addParam($key,$value);
		}
	}
	/**
	 * Loads an array into template engine, optional prefixing each key with $prefix
	 * @param array $lista Key/value hash of template variables. All keys must be string
	 * @param string $prefix Optional prefix will be prepended to all keys. A '_' will be added between prefix and key name
	 */
	public function paramArray(array $lista,$prefix='') {
		if ($prefix) $prefix.='_';
		foreach ($lista as $k=>$v) {
			$this->addParam($prefix.$k,$v);
		}
	}
	/**
	 * Loads an array into template engine, optional prefixing each key with $prefix.
	 * Only loads not existent keys
	 * @param array $lista Key/value hash of template variables. All keys must be string
	 * @param string $prefix Optional prefix will be prepended to all keys. A '_' will be added between prefix and key name
	 */
	public function paramArrayOpt(array $lista,$prefix='') {
		if ($prefix) $prefix.='_';
		foreach ($lista as $k=>$v) {
			if (!$this->hasParam("$prefix$k")) {
				$this->addParam($prefix.$k,$v);
			}
		}
	}
	/**
	 * Loads an array in template
	 * Same as paramArray but prefixes name with "LOOP_"
	 * @param string $nome
	 * @param array $lista
	 * @see paramArray
	 * @deprecated
	 */
	function paramLoop($nome,array $lista) {
		$this->addParam("LOOP_$nome",$lista);
	}
	protected function unQuote($stringa) {
		return  str_replace('"','',$stringa);
	}
	protected function quote(&$param) {
		$param=sprintf('"%s"',$param);
	}
	protected function wrap($stringa) {
		return ('<'."?php $stringa ?".'>');
	}
	/*
	 * Tag Management
	 */
	protected function includeFile($fname,$smart=false,$static=false,$flags=array()) {
		/* Deferring file name resolution to run time context */
		$tag=$flags['_FULL'];
		if ($static) {
			return $this->wrap("\$this->includeStaticFile('$fname','$smart','$tag')");
		}
		else {
			return $this->wrap("\$this->includeParsedFile('$fname','$smart','$tag')");
		}
	}
	protected function includeParsedFile($fname,$smart,$tag) {
		if (substr($fname,0,1)=='/') {
			$fname2=$fname; // absolute name, checking for smartness is pointless
			$this->debug("Absolute Including $fname:",$this->options);
		}
		else {
			$fname2=$this->resolveFile($fname);
			$this->debug("Including $fname:",$this->options);
		}
		$this->tree[]=array(
				'requested'=>$fname,
				'included'=>$fname2,
				'level'=>$this->treelevel
		);
		return $this->parseTemplate($fname2,$tag);
	}
	protected function includeStaticFile($fname,$smart,$tag)
		{
		//--- Avoid to go updir
		$fname = str_replace("../","",$fname);

		if (preg_match("/^([A-Za-z0-9\_\.\/]+|\[[A-Za-z0-9\_]+\])+$/",$fname,$regs)) {

			$fname = preg_replace_callback("/\[([A-Za-z0-9\_]+)\]/",array($this, 'replace_varname'),$fname);
			$fname2=$this->resolveFile($fname);

			$this->debug("Static including $fname:",$this->options);
			$this->tree[]=array(
					'requested'=>$fname,
					'included'=>$fname2,
					'level'=>$this->treelevel
			);
			return $this->includeTemplate($fname2,$tag);
		}
		else
		{
			$this->Error("Invalid static name '$fname'");
			return "Invalid static name '$fname'. Sorry.<br>";
		}
		}

	protected function tmpl_smartinclude($flags) {
		if (empty($flags['NAME'])) {
			$this->debug("TMPL_SMARTINCLUDE without name=: ".htmlspecialchars($flags['_FULL']));
			return 'FAILED INCLUSION';
		}
		return $this->includeFile($flags['NAME'],true,false,$flags);
	}
	protected function tmpl_include($flags) {
		if (empty($flags['NAME'])) {
			$this->debug("TMPL_INCLUDE without name=: ".htmlspecialchars($flags['_FULL']));
			return 'FAILED INCLUSION';
		}
		return $this->includeFile($flags['NAME'],false,false,$flags);
	}

	protected function tmpl_staticinclude($flags) {
		if (empty($flags['NAME'])) {
			$this->debug("TMPL_STATICINCLUDE without name=: ".htmlspecialchars($flags['_FULL']));
			return 'FAILED INCLUSION';
		}
		return $this->includeFile($flags['NAME'],false,true,$flags);
	}

	protected function tmpl_var($flags) {
		$param=$flags['NAME'];
		$escape=$flags['ESCAPE'];
		return $this->wrap("\$this->show('$param','$escape');");
	}
	protected function tmpl_var_xml($flag) {
		$version=$flag['VERSION'];
		$encodinge=$flag['ENCODING'];
		return $this->wrap("echo '<'.'?xml version=\"$version\" encoding=\"$encoding\" ?'.'>';");
	}
	protected function tmpl_if($flags) {
		if ($flags['CLOSE'])
			return $this->wrap('}');
		else {
			$param=$flags['NAME'];
			return $this->wrap("if (\$this->isTrue('$param')) {");
		}
	}
	protected function tmpl_unless($flags) {

		if ($flags['CLOSE'])
			return $this->wrap('}');
		else {
			$param=$flags['NAME'];
			return $this->wrap("if (\$this->isFalse('$param')) {");
		}
	}
	protected function tmpl_else() {
		return $this->wrap(" } else {");
	}
	protected function tmpl_elseif($flags) {
		$param=$flags['NAME'];
		return $this->wrap(" } elseif  (\$this->isTrue('$param')) {");
	}
	protected function tmpl_elseunless($flags) {
		$param=$flags['NAME'];
		return $this->wrap(" } elseif  (\$this->isFalse('$param')) {");
	}
	protected function tmpl_loop($flags) {
		if ($flags['CLOSE']) {
			return $this->wrap('$this->popCurrentContext();}');
		}
		else {
			$param=$flags['NAME'];
			$prefix=$this->getContextPrefix($param);
			$k=$prefix."_key";
			$v=$prefix."_value";
			$d=$prefix."_data";
			return $this->wrap("\$$d=(array) \$this->getVar('$param');\$$k=0;foreach (\$$d as \$$v ) {\$this->pushCurrentContext('$prefix',\$$v,\$$k++,count(\$$d));");
		}
	}

	/**
	 * Data access
	 */

	protected function isTrue($param) {
		return (boolean)$this->getVar($param);
	}
	protected function isFalse($param) {
		return !(boolean)$this->getVar($param);
	}
	protected function getVar($param,$escape='',$show=false) {
		static $criticalSection=false;
		$param=strtoupper($param);
		$k=reset($this->currentContexts);
		if (!is_object($k))
			$k=$this->rootData;
		if (preg_match('/^\_\_([^_]*)\_\_$/U',$param,$match)) {
			$this->debug("getLoopVar: $param");
			if ($show and $this->debugVars) return "[$param]";
			$param=$match[1];
			$c=$k->context($param);
			$this->debug("Context var $param in " . $k->name().": $c",$k->dump());
			return $k->context($param);
		}
		elseif (!strncasecmp($param,$this->dictPrefix,strlen($this->dictPrefix))) {
			$this->debug("getDictVar: $param");
			if ($show and $this->debugDict) return "[$param]";
			$this->debug("Used dictionary key: $param");
			return Node::escape($this->fire(self::DICT,substr($param,strlen($this->dictPrefix)),$k->raw()),$escape);
		}
		else {
			$this->debug("getGenericVar: $param ({$this->dictPrefix})");
			if ($show and $this->debugVars) return "[$param]";
			foreach ($this->currentContexts as $context) {
				if ( $context->has($param)) {
					return $context->get($param,$escape);
				}
			}
		}
		//$this->markInfo("Accessing $param in root data");
		if ($this->rootData->has($param))
			return $this->rootData->get($param,$escape);
		return Node::escape($this->fire(self::MISS,$param,$k->raw()),$escape);
	}

	protected function debugInclude($tag,$result) {
		if (!$this->debugTemplate) return;
		if (!$this->debugInclude) return;
		$this->inlineDebug('<b>' .htmlspecialchars($tag) .":</b> $result");
	}


	protected function show($param,$escape='') {
		echo $this->getVar($param,$escape,true);
	}

	protected function pushCurrentContext($uid,$param,$iteration,$total) {
		$this->Debug(sprintf("Memory before %dK", (memory_get_usage()/1024)));
		array_unshift($this->currentContexts,$this->newNode($uid,$param,$iteration,$total));
	}
	protected function popCurrentContext() {
		array_shift($this->currentContexts);

		$this->Debug(sprintf("Memory after_ %dK", (memory_get_usage()/1024)));

	}
	/*
	 * CompileTime functions
	 */

	protected function getContextPrefix($param) {
		return sprintf("%s_%04d",$param,$this->unique++);
	}
	protected function parseParams($param) {
		if (empty($param)) return array();
		$param=preg_replace(array('/ +/','/ ?= ?/'),array(' ','='),$param); //squeeze
		$coppie=explode(' ',$param);
		$rv=array();
		foreach ($coppie as $coppia) {
			list($k,$v)=explode('=',$coppia);
			if (empty($v)) {
				$v=$k;
				$k='NAME';
			}
			else {
				$k=strtoupper($k);
			}
			$v=preg_replace('/[^\w\/\.\-\]\[\*]/','',$v);
			$rv[$k]=$v;
		}
		return $rv;
	}
	protected function parse($matches) {

		$match=$matches[0];
		$closed=$matches[1];
		$tag=strtoupper($matches[2]);
		$this->debug("TAG=$tag NAME=$NAME ESCAPE=$ESCAPE",$matches);
		$flags=$this->parseParams(trim($matches[3],' /'));
		$this->debug("Matched tag $tag Flags",$flags);
		$flags['CLOSE']=!empty($closed);
		$flags['_FULL']=$match;
		extract($flags,EXTR_SKIP);
		if (!method_exists($this,$tag)) {
			throw new \BadMethodCallException("Method $tag not found");
		}
		return $this->$tag($flags);
	}
	protected function compileTemplate($template,$tag) {
		return $this->parseTemplate($template,$tag);

	}
	protected function getCompiledName($fname) {
		return str_replace('//','/',$this->cacheDir . $fname . '.v'.$this->version.'.php');
	}
	protected function saveCompiledTemplate($fname,$s) {
		$output=$this->getCompiledName($fname);

		@mkdir(dirname($output),0777,true);
		$ok=file_put_contents($output,$this->wrap('/* <!'.gmdate('U').'!> Template cached on ' . gmdate('c') . "*/") . "\n$s");
		if ($ok>0) {
			$this->Success("Saved compiled template to $output");
		}
		else {
			$this->Error("Unable to write $output for writing" ,error_get_last());
		}
	}
	protected function checkCompiledTemplate($filename) {
	    if ($this->resolver->isFresh($filename)) {
	        return $this->resolver->getCompiledfileName($filename);
	    }
		return false;
	}
	protected function _eval($s) {
		$start=microtime(true);
		header("Content-type: text/html");
		try {
		  eval('?'.'>'.$s);
		}
		catch (\Throwable $e ) {
		    echo $e->getMessage();
            foreach (preg_split('/\v/',$s) as $line=>$row) {
                printf("%6d.",$line+1);
                highlight_string($row);
                echo "<br>";
            }
            die();
		}
		$start=microtime(true)-$start;
		$this->evalTime=$this->readingTime+$start;

	}
	protected function file_get_contents($fname) {
		$start=microtime(true);
		$s=@file_get_contents($fname);
		$start=microtime(true)-$start;
		$this->readingTime+=$start;
		return $s;
	}
	protected function file_exists($fname) {
		$start=microtime(true);
		$s=@file_exists($fname);
		$start=microtime(true)-$start;
		$this->readingTime+=$start;
		return $s;
	}
	protected function realpath($fname) {
		$start=microtime(true);
		$s=@realpath($fname);
		$start=microtime(true)-$start;
		$this->readingTime+=$start;
		return $s;
	}
	protected function includeTemplate($template,$originalname='',$tag='<TMPL_STATICINCLUDE>') {
		$this->debug("Starting include $template ");
		$this->debugInclude($originalname,$template,$tag);
		$this->resolver->getTemplate($template);
	}
	protected function parseTemplate($template,$originalname='',$tag='<TMPL_INCLUDE>') {
		$this->debug("Using regexp:" . $this->regExp);
		$this->debugInclude($originalname,$template,$tag);
		$this->debug("Starting compilation for $template ");
		$this->includes[]=$template;
		++$this->treeLevel;
		$cname=$this->checkCompiledTemplate($template);
		$this->debug("Compile name $cname");
		if ($cname) {
			$start=microtime(true);
			include($cname);
			$this->incTime+=(microtime(true)-$start);
			$this->debug("Included $cname from cached php in ".$this->elapsed($start));
			--$this->treeLevel;
			return;
		}
		if ($this->resolver->peekTemplate($template)) {
			$s=$this->resolver->getTemplate($template);
			$start=microtime(true);
			if (substr($s,0,5)=='<?xml') {
				$s=$this->wrap("echo '<?xml';") . substr($s,5);
			}
			$s=preg_replace_callback($this->regExp,array($this,"parse"),$s);
			$this->parsingTime+=(microtime(true)-$start);
			$this->Success("Parsed $template in ".$this->elapsed($start));
			$this->resolver->saveCompiledTemplate($template,$s);
		}
		else {
			$this->Error("Missing template file [$template]");
			$s="<font color='red'>Missing template [$template]</font>";
		}
		/*
		 < : matches initial <
		 (\/?) : matches eventual / (closed tag)
		 (tmpl_[^ >]+) : matches tag name
		 ([^>])* : matches parameters
		 > : matches final >
		 */
		$this->debug("Compilation done $template");
		$this->_eval($s);
		--$this->treeLevel;

	}

	protected function tree() {
		$lista=array();
		foreach($this->tree as $i => $leaf) {
			if (substr($leaf['requested'],0,1)=='/') {
				$color='red';
			}
			else {
				$color='green';
			}
			$lista[sprintf('%03d',$i)]=sprintf('L:%02d',$leaf['level']). str_repeat('  ',$leaf['level']) ."<b><font color=\"$color\">{$leaf['requested']}</font></b> (<i>{$leaf['included']}</i>)";
		}
		return array('includes'=>$this->includes,
					'tree' => $lista,
					'reading time'=>$this->readingTime,
					'parsing time'=>$this->parsingTime,
					'eval time'=>$this->evalTime);
	}

	protected function newNode($uid,$data,$iteration,$total) {
		static $nodes=array();
		//if (!is_object($nodes[$uid])) $nodes[$uid];
		$nodes[$uid]=new Node($uid,$data,$iteration,$total,$this);
		return $nodes[$uid];
	}
	public function elapsed($start) {
		return sprintf("%.5f",microtime(true)-$start);

	}

	protected function loaded() {
		return true;
	}
}
