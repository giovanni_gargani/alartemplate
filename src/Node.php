<?php
namespace Alar\Template;
class Node {
	private $data=array();
	private $uid="0";
	public $iteration=0;
	public $total=0;
	private $engine=null;
	function __construct($uid,$data,$iteration,$total,TemplateInterface $engine) {
		$start=microtime(true);
		$this->uid=$uid;
		$this->data=$data;
		$this->engine=$engine;

		if ($uid!="root") {
			if (!is_array($data)) {
				$this->engine->error("Not an array creating context $uid iteration $iteration",false,$data);
				$this->data=array();
			}
			else {
				if (!$this->engine->caseSensitive)
					$this->data=array_change_key_case($data,CASE_UPPER);
			}
		}
		$this->iteration=$iteration+1;
		$this->total=$total;
		$this->engine->info("Loaded new node for context $uid, iteration $iteration in ".$this->engine->elapsed($start));
	}

	protected function setIteration($iteration) {
		die("iterating");
		$this->iteration=$iteration+1;
		$this->engine->debug("Context $uid: Executing iteration $this->iteration of $this->total");
	}
	protected function name() {
		return $this->uid;
	}
	public function has($key) {
		return array_key_exists(strtoupper($key), $this->data);

	}

	public function get($key,$escape) {
		$rvalue=$this->data[strtoupper($key)];
		if ($escape) {
			$this->engine->info("$key requested with escape: $escape");
		}
		return self::escape($rvalue,$escape);
	}
	protected function dump() {
		return $this->data;
	}
	public static function escape($rvalue,$escape) {
		switch ($escape) {
			case 'HTML':
				return htmlspecialchars($rvalue, ENT_QUOTES);
			case 'JS':
				return addcslashes($rvalue, "\\\n\r\t\"'");
			case 'URL':
				return urlencode($rvalue);
			case '_AS_JSON':
			case 'JSON':
				return json_encode($rvalue);
			case 'AS_CDATA':
			case 'CDATA':
				return '<![CDATA['.$rvalue.']]>';
			default:
				return $rvalue;
		}
	}
	public function raw() {
		return array('uid'=>$this->uid,'iter'=>$this->iteration,'data'=>$this->data);
	}
	protected function context($param) {

		switch (strtoupper($param)) {
			case "FIRST" : return $this->iteration==1;
			case "LAST" : return $this->iteration==$this->total;
			case "INNER" : return $this->iteration > 1 and $this->iteration < $this->total;
			case "COUNTER" : return $this->iteration;
			case "EVEN" : return  ($this->iteration % 2);
			case "ODD" : return  (($this->iteration+1) % 2);
			case "ROW$this->iteration" : return true;
		}
		return false;
	}
}

