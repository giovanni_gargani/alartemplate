<?php
namespace Alar\Template;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerTrait;

trait LogForwarderTrait  {
    use LoggerAwareTrait,LoggerTrait;
    public function log($level, $message, array $context = array()) {
        if ($this->logger) $this->logger->log($level,$message,$context);
    }
}