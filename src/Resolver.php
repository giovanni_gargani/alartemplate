<?php
namespace Alar\Template;

use Psr\Log\LoggerInterface;

class Resolver implements ResolverInterface {
    use LogForwarderTrait;
    use ResolverTrait;
    protected $classVersion=0;
    protected $cacheDir='/tmp/';
    public function __construct(string $version = "0", LoggerInterface $logger = null) {
        $this->cacheDir=rtrim(sys_get_temp_dir(),DIRECTORY_SEPARATOR). DIRECTORY_SEPARATOR; // Makes sure we have one and only one trailing slash 
        $this->setClassVersion($version);
        if ($logger) $this->setLogger($logger);
    }
}