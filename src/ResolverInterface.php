<?php
namespace Alar\Template ;

use Psr\Log\LoggerInterface;

interface ResolverInterface {
    /**
     * Resolves the templatename to a phisical identifier to be used by the access functions
     * @param string $templatename
     * @param string $root optional root folder. Actual implementations can use it as they like  
     * @param array $includePaths optional array of folders to search in. Actual implementations
     * can use it as they like
     * In case of error should store an error message to be retrieved via getLastError()
     * @return string
     */
    public function resolve(string $templatename,string $root='',array $includePaths=null) : string;
    /**
     * Returns the content for the template identified by filename
     * In case of error should store an error message to be retrieved via getLastError()
     * @param string $filename
     */
    public function getTemplate(string $filename): string;
    /**
     * Check if $filename leads to an actual template 
     * @param string $filename
     * In case of error should store an error message to be retrieved via getLastError()
     * @return bool Success
     */
    public function peekTemplate(string $filename): bool;
    /**
     * Return a valid name for the compiled version of the template
     * @param string $filename
     * @return string
     */
    public function getCompiledFilename(string $filename) : string ;
    /**
     * Returns true if the template must be recompiled
     * @param string $filename
     * @param string $compiledFilename
     * @return bool Success 
     */
    public function isFresh(string $filename) : bool;
    /**
     * Stores the compiled template
     * @param string $filename
     * @param string $content
     * In case of error should store an error message to be retrieved via getLastError()
     * @return bool Success 
     */
    public function saveCompiledTemplate(string $filename,string $content): bool;
    /**
     * Stores the Template class version for future use
     * (Should be used to compose the compiled file name)
     * @param int $ts
     */
    public function setClassVersion(string $version);
    /**
     * Class version is used to buil compiled template name in the default (and recommended) implementation
     * Up to implementation using it
     * @param string $ts
     */
    
    public function setCachedir(string $path);
    
    /**
      * Can be implemented as void but must be present
      */
    public function setLogger(LoggerInterface $logger);
    /**
     * Returns the timestamp of the compiled template,
     * If compiled file is missing returns 0
     * @param string $filename
     * @return int timestamp
     */
    public function getCompileTime(string $filename) : int;
    
}
