<?php
namespace Alar\Template;
interface DictionaryInterface extends \IteratorAggregate  {
    /**
     * 
     * @param string $event DICT or MISS constants (from Alar\Template) 
     * @param string $key requested variable
     * @param array $context Current context if inside a loop or rootData otherwise
     * @return mixed The desired value (will be processed by Alar\Node::escape 
     */
	public function get($event,$key,$context);
	
}