# README #

alar/template template engine

### What is this repository for? ###

* alar/template is a template engine compatible with PERL::HtmlTemplate

This project is mostly for internal use so documentation is still largely incomplete, but code should be not to hard to check.


### How do I get set up? ###

* Summary of set up

    composer require alar/template

* Usage


	use alar\template\template;
	$template=new template($options);
	where $options is a hash with this possible keys
		'paths'=>string or array path to search when template name is relative
		'debug'=> boolean debug on/off
		'debuglevel'=> PSR-3 log level
		'novars' => boolean when true, variables name are printed instead of values
		'nodict' => boolean, same as novars but for Dictionary variables,
		'callback'=>dictionaryInterface an object to be used or dictionary access
		'filename'=>string templatename,
		'cachedir'=>string cacheDirectory,
		'tmplroot'=string >base template dir,

